import "./App.css";
import HomePage from "./pages/home/HomePage";
import CartPage from "./pages/cart/CartPage";
import Checkout from "./pages/checkout/Checkout";
import Layout from "./layout/Layout";
import { Routes, Route, useLocation, Navigate } from "react-router-dom";
import Signup from "./pages/signup/Signup";
import Login from "./pages/login/Login";
import OrderPage from "./pages/order/OrderPage";
import Profile from "./pages/profile/Profile";
import Result from "./pages/result/Result";
import Product from "./pages/products/_id/Product_id";
import Products from "./pages/products/Products";
import NotFound from "./pages/notFound/NotFound";
import Index from "./pages/Index";
import { useAuth, useAuthAction } from "./Providers/auth/AuthProvider";
import { useEffect } from "react";
import { userType } from "./Providers/user/UserType";
import { authType } from "./Providers/auth/AuthType";
import { useUserAction } from "./Providers/user/UserProvider";
import { cart_type } from "./Providers/cart/CartType";
import { useCartAction } from "./Providers/cart/CartProvider";

function App() {
  const userDispatch = useUserAction();
  const authDispatch = useAuthAction();
  const cartDispatch = useCartAction();
  const cart = JSON.parse(localStorage.getItem("cart"));

  useEffect(() => {
    const token = localStorage.getItem("token") || "";
    if (token) {
      userDispatch({
        type: userType.LOGIN,
        payload: {
          user: {
            name: localStorage.getItem("name") || "",
            email: localStorage.getItem("email") || "",
            token,
          },
        },
      });
      authDispatch({
        type: authType.AUTH,
        payload: true,
      });
    } else {
      userDispatch({
        type: userType.LOGOUT,
      });
      authDispatch({
        type: authType.AUTH,
        payload: true,
      });
    }

    if (cart) {
      cart.map((item) => {
        cartDispatch({
          type: cart_type.ADD_TO_CARD,
          payload: item,
          toast: false,
        });
      });
    }
  }, []);
  return (
    <Layout>
      <Routes>
        <Route path="/" element={<HomePage />} exact={true} />
        <Route index element={<Index />} />
        <Route path="cart" element={<CartPage />} />
        <Route
          path="checkout"
          element={
            <RequireAuth>
              <Checkout />
            </RequireAuth>
          }
        />
        <Route path="login" element={<Login />} />
        <Route path="signup" element={<Signup />} />
        <Route
          path="profile"
          element={
            <RequireAuth>
              <Profile />
            </RequireAuth>
          }
        />
        <Route
          path="order"
          element={
            <RequireAuth>
              <OrderPage />
            </RequireAuth>
          }
        />
        <Route
          path="result"
          element={
            <RequireAuth>
              <Result />
            </RequireAuth>
          }
        />
        <Route path="product/:id" element={<Product />} />
        <Route path="product" element={<Products />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Layout>
  );
}

export default App;

function RequireAuth({ children }) {
  let { auth } = useAuth();
  let location = useLocation();

  if (!auth) {
    // Redirect them to the /login page, but save the current location they were
    // trying to go to when they were redirected. This allows us to send them
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.
    return <Navigate to="/login" state={{ from: location }} />;
  }

  return children;
}
