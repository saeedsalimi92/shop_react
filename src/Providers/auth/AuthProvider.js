import { useContext, useReducer, createContext } from "react";
import AuthReducer from "./AuthReducer";

const AuthContext = createContext();
const AuthContextDispatcher = createContext();
const INITIAL_STATE = {
  auth: localStorage.getItem("token") ? true : false,
};
const AuthProvider = ({ children }) => {
  const [auth, dispatch] = useReducer(AuthReducer, INITIAL_STATE);
  return (
    <AuthContext.Provider value={auth}>
      <AuthContextDispatcher.Provider value={dispatch}>
        {children}
      </AuthContextDispatcher.Provider>
    </AuthContext.Provider>
  );
};
export default AuthProvider;

export const useAuth = () => useContext(AuthContext);

export const useAuthAction = () => useContext(AuthContextDispatcher);
