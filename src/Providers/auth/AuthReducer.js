import { authType } from "./AuthType";

const AuthReducer = (state, action) => {
  switch (action.type) {
    case authType.AUTH:
      return {
        ...state,
        auth: action.payload,
      };
    default:
      return state;
  }
};
export default AuthReducer;
