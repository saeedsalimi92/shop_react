import { cart_type } from "./CartType";
import { toast } from "react-toastify";

const addToCard = (state, payload, notif) => {
  const updatedCard = [...state.cart];
  const updateItemIndex = updatedCard.findIndex(
    (item) => item.id === payload.id
  );

  if (updateItemIndex < 0) {
    if (notif) {
      toast.success("ProductDetail added", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

    updatedCard.push({ ...payload, quantity: 1 });
  } else {
    const updatedItem = { ...updatedCard[updateItemIndex] };
    updatedItem.quantity++;
    updatedCard[updateItemIndex] = updatedItem;
  }
  localStorage.setItem("cart", JSON.stringify(updatedCard));
  return { ...state, cart: updatedCard, total: state.total + payload.offPrice };
};

const deleteCard = (state, payload) => {
  const updatedCard = [...state.cart];
  const updateItemIndex = updatedCard.findIndex(
    (item) => item.id === payload.id
  );
  const updatedItem = { ...updatedCard[updateItemIndex] };
  if (updatedItem.quantity > 1) {
    updatedItem.quantity--;
    updatedCard[updateItemIndex] = updatedItem;
    localStorage.setItem("cart", JSON.stringify(updatedCard));
    return {
      ...state,
      cart: updatedCard,
      total: state.total - payload.offPrice,
    };
  } else {
    toast.success("ProductDetail removed", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    const filtered = updatedCard.filter((item) => item.id !== payload.id);
    localStorage.setItem("cart", JSON.stringify(filtered));
    return { ...state, cart: filtered, total: state.total - payload.offPrice };
  }
};

const removeFromCard = (state, payload) => {
  const allCards = [...state.cart];
  const filteredCards = allCards.filter((item) => item.id !== payload.id);
  toast.success("ProductDetail removed", {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
  return {
    ...state,
    cart: filteredCards,
    total: state.total - payload.offPrice,
  };
};
const removeAllCart = (state) => {
  return {
    ...state,
    cart: [],
    total: 0,
  };
};

const CartReducer = (state, { type, payload, toast = true }) => {
  switch (type) {
    case cart_type.ADD_TO_CARD:
      return addToCard(state, payload, toast);

    case cart_type.DELETE_ITEM:
      return deleteCard(state, payload, toast);

    case cart_type.REMOVE_FROM_CARD:
      return removeFromCard(state, payload, toast);
    case cart_type.REMOVE_ALL_CART:
      return removeAllCart(state);

    default:
      return state;
  }
};
export default CartReducer;
