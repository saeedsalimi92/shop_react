import { useContext, useReducer, createContext } from "react";
import UserReducer from "./UserReducer";

const UserContext = createContext();
const UserContextDispatcher = createContext();
const INITIAL_STATE = {
  user: {
    name: "",
    email: "",
    token: "",
  },
};
const UserProvider = ({ children }) => {
  const [user, dispatch] = useReducer(UserReducer, INITIAL_STATE);
  return (
    <UserContext.Provider value={user}>
      <UserContextDispatcher.Provider value={dispatch}>
        {children}
      </UserContextDispatcher.Provider>
    </UserContext.Provider>
  );
};
export default UserProvider;

export const useUser = () => useContext(UserContext);

export const useUserAction = () => useContext(UserContextDispatcher);
