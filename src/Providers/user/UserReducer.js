import { userType } from "./UserType";
const random = (min, max) => {
  return Math.random() * (max - min) + min;
};
const signup = (state, payload) => {
  const updateUser = payload.user;
  const token = random(1000000, 10000000);
  updateUser.token = token;
  localStorage.setItem("name", payload.user.name);
  localStorage.setItem("email", payload.user.email);
  localStorage.setItem("token", token);

  return { ...state, user: updateUser };
};

const login = (state, payload) => {
  const updateUser = payload.user;
  const token = random(1000000, 10000000);
  updateUser.token = token;
  localStorage.setItem("name", "User");
  localStorage.setItem("email", payload.user.email);
  localStorage.setItem("token", token);
  return { ...state, user: updateUser };
};

const logout = (state) => {
  localStorage.removeItem("name");
  localStorage.removeItem("email");
  localStorage.removeItem("token");
  return {
    ...state,
    user: {
      name: "",
      email: "",
      token: "",
    },
  };
};

const UserReducer = (state, action) => {
  switch (action.type) {
    case userType.LOGIN:
      return login(state, action.payload);

    case userType.SIGNUP:
      return signup(state, action.payload);

    case userType.LOGOUT:
      return logout(state, action.payload);

    default:
      return state;
  }
};
export default UserReducer;
