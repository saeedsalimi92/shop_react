import { NavLink, useNavigate } from "react-router-dom";
import Styles from "./Navigation.module.css";
import { useCart } from "../../Providers/cart/CartProvider";
import { useAuth, useAuthAction } from "../../Providers/auth/AuthProvider";
import { useUser, useUserAction } from "../../Providers/user/UserProvider";
import { userType } from "../../Providers/user/UserType";
import { authType } from "../../Providers/auth/AuthType";
import { Row, Col, Dropdown, Button, Badge } from "react-bootstrap";
import { FaShoppingCart } from "react-icons/fa";

const Navigation = () => {
  const { user } = useUser();
  const { cart } = useCart();
  const { auth } = useAuth();
  const userDispatch = useUserAction();
  const authDispatch = useAuthAction();
  let Navigate = useNavigate();
  const profile = (event) => {
    if (parseInt(event) === 1) {
      /* Navigate("/profile");*/
    } else if (parseInt(event) === 2) {
      Navigate("/order");
    } else if (parseInt(event) === 3) {
      logout();
    }
  };
  const logout = () => {
    userDispatch({
      type: userType.LOGOUT,
    });
    authDispatch({
      type: authType.AUTH,
      payload: false,
    });
    Navigate("/login");
  };
  return (
    <header className={Styles.mainNavigation}>
      <nav>
        <Row className={`${Styles.nav} m-0 align-items-center`}>
          <Col
            sm={3}
            md={4}
            lg={4}
            xl={3}
            xs={12}
            className="text-center fw-bold "
          >
            <NavLink to="/" activeclassname="active-link" exact="true">
              Shop Site
            </NavLink>
          </Col>
          <Col
            sm={3}
            md={4}
            lg={4}
            xl={6}
            className="text-center fw-bold d-none d-sm-block"
          >
            <div className="d-flex">
              <div className={Styles.item}>
                <NavLink
                  to="/product"
                  activeclassname="active-link"
                  exact="true"
                >
                  Products
                </NavLink>
              </div>
            </div>
          </Col>
          <Col
            sm={6}
            md={4}
            lg={4}
            xl={3}
            className="text-center fw-bold d-none d-sm-block"
          >
            <div className="d-flex align-items-center">
              <div className={Styles.item}>
                <NavLink to="/cart" activeclassname="active-link">
                  <Button className={Styles.btn}>
                    <FaShoppingCart />
                    {cart.length > 0 && (
                      <Badge className={Styles.badge}>{cart.length}</Badge>
                    )}
                    <span className="visually-hidden">unread messages</span>
                  </Button>
                </NavLink>
              </div>
              <div className={Styles.item}>
                {auth ? (
                  <>
                    <Dropdown onSelect={(e) => profile(e)}>
                      <Dropdown.Toggle
                        id="dropdown-basic"
                        className={`${Styles.navigationBtn} custom-drop`}
                      >
                        {user.name}
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        {/*<Dropdown.Item eventKey={1} as="button">
                          profile
                        </Dropdown.Item>*/}
                        <Dropdown.Item eventKey={2} as="button">
                          OrderList
                        </Dropdown.Item>
                        <Dropdown.Item eventKey={3} as="button">
                          Logout
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </>
                ) : (
                  <NavLink to="/login" activeclassname="active-link">
                    Login / signup
                  </NavLink>
                )}
              </div>
            </div>
          </Col>
        </Row>
      </nav>
    </header>
  );
};
export default Navigation;
