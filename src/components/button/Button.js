import Styles from "./Button.module.css";
const Button = ({
  block,
  color,
  title,
  back,
  clickHandler,
  disabled,
  type = "button",
}) => {
  const onClick = () => {
    clickHandler();
  };
  return (
    <button
      className={`${Styles.button} ${disabled && Styles.disabled}`}
      disabled={disabled}
      type={type}
      onClick={() => onClick()}
      style={{
        backgroundColor: back,
        width: block ? "100%" : "auto",
        color,
      }}
    >
      {title}
    </button>
  );
};
export default Button;
