import Styles from "./Cart.Module.css";
import { FaTrash } from "react-icons/fa";
import { Row, Col } from "react-bootstrap";
const Cart = (item) => {
  const {
    image,
    name,
    offPrice,
    quantity,
    incrementHandler,
    decrementHandler,
    removeHandler,
  } = item;
  return (
    <Row className={Styles.box}>
      <Col xs={12} sm={2} md={2}>
        <div className={Styles.imgBox}>
          <img className={Styles.img} src={image} alt={name} />
        </div>
      </Col>
      <Col xs={12} md={6}>
        <div className={Styles.priceSection}>
          <p className={Styles.noPadding}>{name}</p>
          <p className={Styles.noPadding}>{offPrice * quantity} $ </p>
        </div>
      </Col>
      <Col xs={12} md={4}>
        <div className={Styles.actionSection}>
          <button className={Styles.button} onClick={() => removeHandler()}>
            <FaTrash />
          </button>
          <button className={Styles.button} onClick={() => incrementHandler()}>
            +
          </button>
          <span>{quantity}</span>
          <button className={Styles.button} onClick={() => decrementHandler()}>
            -
          </button>
        </div>
      </Col>
    </Row>
  );
};
export default Cart;
