import Styles from "./Input.module.css";
import "./input.scss";
/*import _ from "lodash";*/
const Input = ({ type, label, name, formik }) => {
  return (
    <>
      <div className="form__group field">
        <input
          className="form__field"
          autoComplete="off"
          id={name}
          type={type}
          name={name}
          {...formik.getFieldProps(name)}
          required
        />
        <label
          htmlFor="name"
          className={`form__label ${
            formik.values[name].length ? Styles.top : ""
          }`}
        >
          {label}
        </label>
      </div>
      <div className={Styles.error}>
        {formik.errors[name] && formik.touched[name] && formik.errors[name]}
      </div>
      {/*<div>
        <label className={Styles.label} htmlFor={name}>
          {label} :
        </label>
      </div>

      <div className={Styles.inputBox}>
        <input
          id={name}
          className={Styles.input}
          type={type}
          {...formik.getFieldProps(name)}
          name={name}
        />
        <div className={Styles.error}>
          {formik.errors[name] && formik.touched[name] && formik.errors[name]}
        </div>
      </div>*/}
    </>
  );
};
export default Input;
