import Styles from "./Order.Module.css";
import { FaTrash } from "react-icons/fa";
import { Row, Col } from "react-bootstrap";
const Order = (item) => {
  const { name, price, order, date } = item;
  return (
    <Row className={Styles.box}>
      <Col xs={12} md={12}>
        <Row className={Styles.priceSection}>
          <Col xs={12} sm={6} md={3}>
            <p className={Styles.noPadding}>
              orderNo :<span className="fw-bold">{order}</span>
            </p>
          </Col>
          <Col xs={12} sm={6} md={3}>
            <p className={Styles.noPadding}>
              name :<span className="fw-bold">{name}</span>
            </p>
          </Col>
          <Col xs={12} sm={6} md={3}>
            <p className={Styles.noPadding}>
              price :<span className="fw-bold">{price} $</span>
            </p>
          </Col>
          <Col xs={12} sm={6} md={3}>
            <p className={Styles.noPadding}>
              time :<span className="fw-bold">{date}</span>
            </p>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default Order;
