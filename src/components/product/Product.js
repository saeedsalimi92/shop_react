import Styles from "./Product.module.css";
import { CheckInCart } from "../../utils/CheckInCart";
import { Link } from "react-router-dom";
import Button from "../button/Button";

const Product = ({ product, cart, clickHandler }) => {
  const onClick = (product) => {
    clickHandler(product);
  };
  return (
    <section className={Styles.product}>
      <Link to={`/product/${product.id}`}>
        <div>
          <img
            className={Styles.image}
            src={product.image}
            alt={product.name}
          />
        </div>
      </Link>

      <div className={Styles.body}>
        <Link to={`/product/${product.id}`}>
          <p className={Styles.title}> {product.name}</p>
        </Link>
        <div className={Styles.boxPrice}>
          <p className={Styles.price}>
            <Link to={`/product/${product.id}`}>{product.price} $ </Link>
          </p>

          {CheckInCart(cart, product) ? (
            <Link className={Styles.link} to="/cart">
              in cart
            </Link>
          ) : (
            <Button
              clickHandler={() => onClick(product)}
              disabled={false}
              color="white"
              back="#9d174d"
              title="Add to card"
              block={true}
            />
          )}
        </div>
      </div>
    </section>
  );
};
export default Product;
