import Styles from "./ProductDetail.module.css";
import { CheckInCart } from "../../../utils/CheckInCart";
import { Link } from "react-router-dom";
import Button from "../../button/Button";

const ProductDetail = ({ product, cart, clickHandler }) => {
  const onClick = (product) => {
    clickHandler(product);
  };
  return (
    <section className={Styles.product}>
      <div className="text-center">
        <img className={Styles.image} src={product.image} alt={product.name} />
      </div>

      <div className={Styles.body}>
        <div className={Styles.title}>
          <span>{product.name}</span>
          <span>{product.price} $</span>
        </div>
        <div className={Styles.support}>
          {product.description.map((item, index) => {
            return (
              <div
                className={`${Styles.item} ${
                  (index + 1) % 2 !== 0 && Styles.odd
                }`}
              >
                {item.support}
              </div>
            );
          })}
        </div>

        {CheckInCart(cart, product) ? (
          <Link className={Styles.link} to="/checkout">
            in cart
          </Link>
        ) : (
          <Button
            clickHandler={() => onClick(product)}
            disabled={false}
            color="white"
            back="green"
            title="Add to card"
            block={true}
          />
        )}
      </div>
    </section>
  );
};
export default ProductDetail;
