/*import bcrypt from "bcryptjs";*/

export const products = [
  {
    id: 1,
    name: "Winter body",
    category: "shoes",
    description: [
      { support: "گارانتی مادام العمر" },
      { support: "ارسال رایگان" },
      { support: "اورجینال" },
    ],
    price: 120,
    offPrice: 120,
    discount: 0,
    image: "https://s4.uupload.ir/files/item1_soj.jpg",
  },
  {
    id: 2,
    name: "Adidas",
    category: "shoes",
    description: [{ support: "گارانتی مادام العمر" }, { support: "اورجینال" }],
    price: 110,
    offPrice: 100,
    discount: 8,
    image: "https://s4.uupload.ir/files/item2_fc5s.jpg",
  },
  {
    id: 3,
    name: "Vans",
    category: "shoes",
    description: [
      { support: "گارانتی مادام العمر" },
      { support: "اورجینال" },
      { support: "ارسال رایگان" },
    ],
    price: 99,
    offPrice: 89,
    discount: 10,
    image: "https://s4.uupload.ir/files/item3_zvc9.jpg",
  },
  {
    id: 4,
    name: "White",
    category: "shoes",
    description: [
      { support: "گارانتی مادام العمر" },
      { support: "اورجینال" },
      { support: "ارسال رایگان" },
    ],
    price: 260,
    offPrice: 220,
    discount: 15,
    image: "https://s4.uupload.ir/files/item4_zr94.jpg",
  },
  {
    id: 5,
    name: "Croopped-shoe",
    category: "shoes",
    description: [
      { support: "گارانتی مادام العمر" },
      { support: "اورجینال" },
      { support: "ارسال رایگان" },
    ],
    price: 150,
    offPrice: 150,
    discount: 0,
    image: "https://s4.uupload.ir/files/item5_24ye.jpg",
  },
  {
    id: 6,
    name: "Blues",
    category: "shoes",
    description: [
      { support: "گارانتی مادام العمر" },
      { support: "اورجینال" },
      { support: "ارسال رایگان" },
    ],
    price: 220,
    offPrice: 200,
    discount: 13,
    image: "https://s4.uupload.ir/files/item6_7jfy.jpg",
  },
  {
    id: 7,
    name: "Winter body",
    category: "clothes",
    description: [
      { support: "گارانتی مادام العمر" },
      { support: "ارسال رایگان" },
      { support: "اورجینال" },
    ],
    price: 120,
    offPrice: 120,
    discount: 0,
    image:
      "https://dkstatics-public.digikala.com/digikala-products/7b1e774827fd4dc4efea62fb1f25345cebb229af_1597527801.jpg?x-oss-process=image/resize,m_lfit,h_600,w_600/quality,q_80",
  },
  {
    id: 8,
    name: "Adidas",
    category: "clothes",
    description: [{ support: "گارانتی مادام العمر" }, { support: "اورجینال" }],
    price: 110,
    offPrice: 100,
    discount: 8,
    image:
      "https://dkstatics-public.digikala.com/digikala-products/117355808.jpg?x-oss-process=image/resize,h_1600/quality,q_80",
  },
  {
    id: 9,
    name: "Vans",
    category: "clothes",
    description: [
      { support: "گارانتی مادام العمر" },
      { support: "اورجینال" },
      { support: "ارسال رایگان" },
    ],
    price: 99,
    offPrice: 89,
    discount: 10,
    image:
      "https://dkstatics-public.digikala.com/digikala-products/116723980.jpg?x-oss-process=image/resize,h_1600/quality,q_80",
  },
];
export const orders = [
  {
    id: 1,
    name: "User",
    price: 2500,
    date: "2021/10/14 15:30",
    order: 12354,
  },
  {
    id: 2,
    name: "User",
    price: 6700,
    date: "2021/12/09 08:30",
    order: 12587,
  },
];

/*export const users = [
    {
        name: "Saheb mohamadi",
        email: "saheb.ex@gmail.com",
        password: bcrypt.hashSync("12345", 8),
        phoneNumber: "09180000000",
        isAdmin: true,
    },
    {
        name: "John",
        email: "user@example.com",
        password: bcrypt.hashSync("1234", 8),
        isAdmin: false,
        phoneNumber: "09181230000",
    },
];*/
