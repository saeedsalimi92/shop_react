import Navigation from "../components/Navigation/Navigation";
import { useAuth } from "../Providers/auth/AuthProvider";
import { Navigate } from "react-router-dom";

const Layout = ({ children }) => {
  let auth = useAuth();
  const child = () => {
    if (!auth) {
      return <Navigate to="/login" />;
    }
    return children;
  };

  return (
    <>
      <Navigation />
      <main>{child()}</main>
    </>
  );
};
export default Layout;
