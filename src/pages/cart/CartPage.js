import { useCart, useCartAction } from "../../Providers/cart/CartProvider";
import { cart_type } from "../../Providers/cart/CartType";
import { Col, Row } from "react-bootstrap";
import Cart from "../../components/cart/Cart";
import Styles from "./CartPage.Module.css";
import Button from "../../components/button/Button";
import { Link } from "react-router-dom";

const CartPage = () => {
  const { cart, total } = useCart();
  const dispatch = useCartAction();
  const removeItem = (item) => {
    dispatch({ type: cart_type.REMOVE_FROM_CARD, payload: item });
  };
  const incrementItem = (item) => {
    dispatch({ type: cart_type.ADD_TO_CARD, payload: item });
  };
  const decrementItem = (item) => {
    dispatch({ type: cart_type.DELETE_ITEM, payload: item });
  };
  if (!cart.length) {
    return (
      <Row className="justify-content-center text-center m-0 mt-5">
        <Col sm={12}>
          <h2>no item in cart!</h2>
        </Col>
      </Row>
    );
  } else {
    return (
      <Row className="justify-content-center m-0">
        <Col sm={12} md={12} lg={12} xl={10} xxl={9}>
          <Row className="m-0 justify-content-center">
            <Col xs={12} sm={9} md={8} lg={8} xl={8}>
              <section className="mt-3 m-3">
                {cart.map((item) => {
                  return (
                    <Cart
                      {...item}
                      key={item.id}
                      removeHandler={() => removeItem(item)}
                      incrementHandler={() => incrementItem(item)}
                      decrementHandler={() => decrementItem(item)}
                    />
                  );
                })}
              </section>
            </Col>
            <Col xs={12} sm={9} md={4}>
              <CartSummery total={total} cart={cart} />
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
};
export default CartPage;

export const CartSummery = ({ cart, total }) => {
  const totalPrice = cart.length
    ? cart.reduce((acc, curr) => acc + curr.quantity * curr.price, 0)
    : 0;
  return (
    <section className={`${Styles.box} mt-3 m-1`}>
      <div className={Styles.item}>
        <div className={Styles.title}>Total Price</div>
        <div className={Styles.price}>{totalPrice} $</div>
      </div>
      <hr />
      <div className={Styles.item}>
        <div className={Styles.title}>Total Off </div>
        <div className={Styles.price}>{totalPrice - total} $</div>
      </div>
      <hr />
      <div className={Styles.item}>
        <div className={Styles.title}>Final Price</div>
        <div className={`${Styles.price} ${Styles.totalPrice}`}>{total} $</div>
      </div>
      <div className="mt-3">
        <Link to={`/checkout?price=${total}`}>
          <Button
            block={true}
            title="Pay"
            disabled={false}
            color="white"
            back="#9d174d"
            clickHandler={() => {}}
          />
        </Link>
      </div>
    </section>
  );
};
