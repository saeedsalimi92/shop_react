import { Col, Row } from "react-bootstrap";
import Styles from "../products/Products.module.css";
import Button from "../../components/button/Button";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useCartAction } from "../../Providers/cart/CartProvider";
import { cart_type } from "../../Providers/cart/CartType";

const Checkout = () => {
  let Navigate = useNavigate();
  let [searchParams] = useSearchParams();
  let price = searchParams.get("price");
  const dispatch = useCartAction();
  const clickHandler = (props) => {
    if (props === "success") {
      dispatch({ type: cart_type.REMOVE_ALL_CART });
      Navigate(`/result?pay=success&price=${price}`);
    } else {
      dispatch({ type: cart_type.REMOVE_ALL_CART });
      Navigate(`/result?pay=failed&price=${price}`);
    }
  };
  return (
    <Row className="justify-content-center m-0 mt-5">
      <Col xs={12} sm={12} md={9} lg={6} xl={6} xxl={6} className={Styles.box}>
        <Row className="m-0 justify-content-center">
          <Col xs={12} className={`${Styles.title} mb-5 text-center`}>
            Result Pay
          </Col>
          <Col xs={6}>
            <Button
              block={true}
              disabled={false}
              color="white"
              back="green"
              clickHandler={() => clickHandler("success")}
              title="Success"
            />
          </Col>
          <Col xs={6}>
            <Button
              block={true}
              disabled={false}
              color="white"
              back="red"
              clickHandler={() => clickHandler("failed")}
              title="Failed"
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default Checkout;
