import * as data from "../../data";

import { useCart, useCartAction } from "../../Providers/cart/CartProvider";
import Product from "../../components/product/Product";
import { Link, Outlet } from "react-router-dom";

import { cart_type } from "../../Providers/cart/CartType";
import { Col, Row } from "react-bootstrap";
import Styles from "./HomePage.module.css";
import image from "../../assets/1.jpg";
import image1 from "../../assets/download.png";
import image2 from "../../assets/3.webp";
import { FaAngleRight } from "react-icons/fa";

const HomePage = () => {
  const dispatch = useCartAction();
  const { cart } = useCart();
  const addItem = (product) => {
    dispatch({ type: cart_type.ADD_TO_CARD, payload: product });
  };
  return (
    <Row className="justify-content-center m-0 mt-5">
      <Col xs={12} md={8} className="mb-2">
        <img src={image} alt="" className={Styles.image} />
      </Col>
      <Col xs={12} md={4} className="mb-2">
        <img src={image1} alt="" className={Styles.image} />
      </Col>
      <Col xs={12} md={12} className="mb-2">
        <img src={image2} alt="" className={`${Styles.endImage} mb-5`} />
      </Col>
      <Col xs={12}>
        <Row className="justify-content-between m-0">
          <Col className={Styles.title}>New Product</Col>
          <Col className={Styles.more}>
            <Link to="/product">
              More <FaAngleRight />
            </Link>
          </Col>
        </Row>
      </Col>
      <Col xs={12}>
        <Row className="justify-content-center">
          {data.products.slice(0, 4).map((product) => {
            return (
              <Col xs={12} sm={9} md={6} lg={4} xl={3} key={product.id}>
                <Product product={product} cart={cart} clickHandler={addItem} />
              </Col>
            );
          })}
        </Row>
      </Col>
    </Row>
  );
};
export default HomePage;
