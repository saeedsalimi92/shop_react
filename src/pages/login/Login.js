import Input from "../../components/input/Input";
import Styles from "./Login.module.css";
import Button from "../../components/button/Button";
import { useFormik } from "formik";
import * as Yup from "yup";
import { toast } from "react-toastify";
import { Link, useNavigate } from "react-router-dom";
import { useAuthAction } from "../../Providers/auth/AuthProvider";
import { useUserAction } from "../../Providers/user/UserProvider";
import { authType } from "../../Providers/auth/AuthType";
import { userType } from "../../Providers/user/UserType";
const Login = ({ history }) => {
  const userDispatch = useUserAction();
  const authDispatch = useAuthAction();
  let navigate = useNavigate();
  const initialValues = {
    email: "",
    password: "",
  };
  const validationSchema = Yup.object({
    email: Yup.string()
      .required("email is required")
      .email()
      .min(6, "email is not valid"),
    password: Yup.string()
      .required("password is required")
      .min(6, "password is not valid"),
  });
  const onSubmit = (props) => {
    userDispatch({
      type: userType.LOGIN,
      payload: {
        user: props,
      },
    });
    authDispatch({
      type: authType.AUTH,
      payload: true,
    });
    toast.success("welcome", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    navigate("/");

    formik.resetForm();
  };
  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
    validateOnMount: true,
  });

  return (
    <div className={Styles.box}>
      <div className={Styles.loginBox}>
        <h3 className={Styles.loginTitle} onClick={() => onSubmit}>
          Login
        </h3>
        <form onSubmit={formik.handleSubmit}>
          <Input
            formik={formik}
            type="email"
            label="email"
            name="email"
            changeHandler={formik.handleChange}
          />
          <Input
            formik={formik}
            type="password"
            label="password"
            name="password"
            changeHandler={formik.handleChange}
          />
          <Button
            title="Login"
            block={true}
            back="#9D174D"
            color="white"
            type="submit"
            disabled={!formik.isValid}
            clickHandler={() => onSubmit}
          />
        </form>
        <span className={Styles.subText}>
          I have an Account ,
          <Link className={Styles.link} to="/signup">
            Signup.
          </Link>
        </span>
      </div>
    </div>
  );
};
export default Login;
