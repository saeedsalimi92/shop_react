import { Col, Row } from "react-bootstrap";
import Order from "../../components/order/Order";
import * as data from "../../data";

const OrderPage = () => {
  return (
    <Row className="justify-content-center m-0">
      <Col sm={12} md={12} lg={12} xl={10} xxl={9}>
        <Row className="m-0 justify-content-center">
          <Col xs={12} sm={12} md={12} lg={12} xl={12}>
            <section className="mt-3 m-3">
              {data.orders.map((item) => {
                return <Order {...item} key={item.id} />;
              })}
            </section>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default OrderPage;
