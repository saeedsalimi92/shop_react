import * as data from "../../data";
import Styles from "./Products.module.css";
import { useCart, useCartAction } from "../../Providers/cart/CartProvider";
import Product from "../../components/product/Product";
import { useNavigate, useSearchParams } from "react-router-dom";

import { cart_type } from "../../Providers/cart/CartType";
import { Col, Row } from "react-bootstrap";
import { useEffect, useState } from "react";

const Products = () => {
  const dispatch = useCartAction();
  let [searchParams] = useSearchParams();
  let isActive = searchParams.get("cat");
  if (!isActive) {
    isActive = "all";
  }
  const [products, setProducts] = useState(data.products);

  let Navigate = useNavigate();
  const { cart } = useCart();
  const filterHandler = (filter) => {
    Navigate(`?cat=${filter}`);
  };

  const addItem = (product) => {
    dispatch({ type: cart_type.ADD_TO_CARD, payload: product });
  };
  const initData = () => {
    const allProducts = [...data.products];

    let filteredProducts = allProducts.filter(
      (item) => item.category === isActive
    );
    if (isActive === "all") {
      filteredProducts = allProducts;
    }
    setProducts(filteredProducts);
  };
  useEffect(() => {
    initData();
  }, [isActive]);
  return (
    <Row className="justify-content-center m-0">
      <Col sm={12} md={12} lg={12} xl={12} xxl={12}>
        <Row className="m-0 justify-content-center">
          <Col xs={12} sm={12} md={4} lg={3}>
            <section className={`${Styles.box} mt-3 m-1`}>
              <div className={Styles.title}>filter</div>
              <hr />
              <div
                onClick={() => filterHandler("clothes")}
                className={`${Styles.item} ${
                  isActive === "clothes" && Styles.isActive
                }`}
              >
                clothes
              </div>
              <div
                onClick={() => filterHandler("shoes")}
                className={`${Styles.item} ${
                  isActive === "shoes" && Styles.isActive
                }`}
              >
                shoes
              </div>
              <div
                onClick={() => filterHandler("all")}
                className={Styles.lastItem}
              >
                clear all filters
              </div>
            </section>
          </Col>
          <Col xs={12} sm={12} md={8} lg={9} xl={9}>
            <Row className="justify-content-center">
              {products.map((product) => {
                return (
                  <Col
                    xs={12}
                    sm={6}
                    md={9}
                    lg={6}
                    xl={4}
                    className="text-center"
                  >
                    <Product
                      product={product}
                      key={product.id}
                      cart={cart}
                      clickHandler={addItem}
                    />
                  </Col>
                );
              })}
            </Row>
            {/*       <section className="mt-3 m-3">
                {cart.map((item) => {
                  return (
                      <Cart
                          {...item}
                          key={item.id}
                          removeHandler={() => removeItem(item)}
                          incrementHandler={() => incrementItem(item)}
                          decrementHandler={() => decrementItem(item)}
                      />
                  );
                })}
              </section>*/}
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default Products;
