import { Col, Row } from "react-bootstrap";
import Styles from "./Product_id.Module.css";
import * as data from "../../../data";
import ProductDetail from "../../../components/product/ProductDetail/ProductDetail";
import { useState } from "react";
import { useLocation, useParams } from "react-router-dom";
import { useCart, useCartAction } from "../../../Providers/cart/CartProvider";
import { cart_type } from "../../../Providers/cart/CartType";

const Product_id = () => {
  const { cart } = useCart();
  const dispatch = useCartAction();
  let params = useParams();
  const productData = data.products.find(
    (product) => product.id === parseInt(params.id)
  );
  if (productData) {
  }
  const clickHandler = () => {
    dispatch({ type: cart_type.ADD_TO_CARD, payload: productData });
  };
  return (
    <Row className="justify-content-center m-0">
      <Col sm={12} md={12} lg={12} xl={12} xxl={12}>
        <Row className="m-0 justify-content-center">
          <Col xs={12} sm={10} md={8} lg={7} xl={7}>
            <ProductDetail
              product={productData}
              cart={cart}
              clickHandler={clickHandler}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default Product_id;
