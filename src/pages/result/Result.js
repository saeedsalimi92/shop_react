import { Col, Row } from "react-bootstrap";
import Styles from "./Result.module.css";

import { useSearchParams } from "react-router-dom";

const Result = () => {
  let [searchParams] = useSearchParams();
  let pay = searchParams.get("pay");
  let price = searchParams.get("price");

  return (
    <Row className="justify-content-center m-0 mt-5">
      <Col xs={12} sm={12} md={9} lg={8} xl={7} xxl={7} className={Styles.box}>
        <Row className="m-0 justify-content-center">
          <Col xs={12} className={`${Styles.title} mb-5 text-center`}>
            Result Pay
          </Col>
          <Col xs={12} sm={6} md={6} lg={6} xl={6} xxl={6}>
            <span className={Styles.item}>result:</span>
            <span className={Styles.lastItem}>
              {pay === "success" ? "success" : "failed"}
            </span>
          </Col>
          <Col xs={12} sm={6} md={6} lg={6} xl={6} xxl={6}>
            <span className={Styles.item}>price :</span>
            <span className={Styles.lastItem}>{price} $</span>
          </Col>
          <Col xs={12} sm={6} md={6} lg={6} xl={6} xxl={6}>
            <span className={Styles.item}>orderNo: </span>
            <span className={Styles.lastItem}>
              {Math.round(Math.random() * (10000 - 1000) + 1000)}
            </span>
          </Col>
          <Col xs={12} sm={6} md={6} lg={6} xl={6} xxl={6}>
            <span className={Styles.item}>date:</span>
            <span className={Styles.lastItem}>
              {new Date().toLocaleString()}
            </span>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default Result;
