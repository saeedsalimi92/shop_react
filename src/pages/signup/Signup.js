import Input from "../../components/input/Input";
import Styles from "../login/Login.module.css";
import Button from "../../components/button/Button";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { useUserAction } from "../../Providers/user/UserProvider";
import { useAuthAction } from "../../Providers/auth/AuthProvider";
import { userType } from "../../Providers/user/UserType";
import { authType } from "../../Providers/auth/AuthType";

const Login = () => {
  let navigate = useNavigate();
  const authDispatch = useAuthAction();
  const userDispatch = useUserAction();
  const initialValues = {
    email: "",
    name: "",
    password: "",
  };
  const validationSchema = Yup.object({
    name: Yup.string()
      .required("name is required")
      .min(3, "name must be more than 3 character"),
    email: Yup.string().required("email is required").email(),
    password: Yup.string()
      .required("password is required")
      .min(6, "password is not valid"),
  });
  const onSubmit = (props) => {
    userDispatch({
      type: userType.SIGNUP,
      payload: {
        user: props,
      },
    });
    authDispatch({
      type: authType.AUTH,
      payload: true,
    });

    toast.success("welcome", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

    navigate("/");
    formik.resetForm();
  };
  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
    validateOnMount: true,
  });

  return (
    <div className={Styles.box}>
      <div className={Styles.loginBox}>
        <h3 className={Styles.loginTitle}>Signup</h3>
        <form onSubmit={formik.handleSubmit}>
          <Input
            formik={formik}
            type="text"
            label="name"
            name="name"
            changeHandler={formik.handleChange}
          />
          <Input
            formik={formik}
            type="email"
            label="email"
            name="email"
            changeHandler={formik.handleChange}
          />
          <Input
            formik={formik}
            type="password"
            label="password"
            name="password"
            changeHandler={formik.handleChange}
          />
          <Button
            title="Signup"
            block={true}
            back="green"
            color="white"
            disabled={!formik.isValid}
            clickHandler={() => onSubmit}
          />
        </form>
        <span className={Styles.subText}>
          I don't have an Account ,
          <Link className={Styles.link} to="/login">
            Login.
          </Link>
        </span>
      </div>
    </div>
  );
};
export default Login;
