import axios from "axios";
var MockAdapter = require("axios-mock-adapter");

var mock = new MockAdapter(axios);
export default mock;
